package jp.alhinc.yamazaki_masaya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * クラス名	：CalculateSales
 * 作成日	：2020/04/03
 * 更新日	：2020/04/06
 * 作成者	：山﨑 雅也
 *
 * */
public class CalculateSales {

	// 定数：支店定義ファイル名称
	private final static String BRANCH_DEFINITION_FILE = "branch.lst";

	// 定数：売上集計ファイル名称
	private final static String AGGREGATION_BY_BRANCH = "branch.out";

	public static void main(String args[]) {

		// 支店定義ファイルの中身を格納
		// key：支店コード
		Map<String, String> branchDefinitionMap = new HashMap<>();

		// 支店の売り上げファイルの中身を格納
		ArrayList<String[]> selseArrayList = new ArrayList<String[]>();

		// 支店の売り上げを格納
		// key：支店コード
		Map<String, Integer> salseMap = new HashMap<>();

		// コマンドライン引数に値が渡されているか判定
		if (args.length==0) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// ファイル読み込み処理
		if (!(inputFileData(args[0], branchDefinitionMap, selseArrayList))) {
			return;
		}

		// ファイル集計処理
		if (!(aggregateBranchSalse(selseArrayList, salseMap))) {

			return;
		}

		// ファイル出力処理
		if (!(outputFileData(args[0], branchDefinitionMap, salseMap))) {

			return;
		}

	}

	/**
	 * 〈支店定義ファイル〉〈支店売上ファイル〉読み込みメソッド
	 * @param String ファイルの格納先
	 * @param Map<String,String> 支店定義ファイル用Mapの参照渡し
	 * @param ArrayList<String[]> 売上ファイル用Listの参照渡し
	 *
	 * @return boolean 処理結果の有無
	 */
	private static boolean inputFileData(String address, Map<String, String> branchDefinitionMap,
			ArrayList<String[]> salseArrayList) {

		File branchDefinitionFile = new File(address, BRANCH_DEFINITION_FILE);

		// 支店定義ファイルが存在しているか判定
		if (!(branchDefinitionFile.exists())) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}

		BufferedReader br = null;
		try {

			br = new BufferedReader(new FileReader(branchDefinitionFile));

			// 支店定義ファイルのレコード情報を格納
			String branchDefinitionData;
			while ((branchDefinitionData = br.readLine()) != null) {

				// カンマ区切りで入力されたデータを配列に格納
				String[] inputData = branchDefinitionData.split(",");

				// 支店コードが数字３桁か判定
				if (!(inputData[0].matches("[0-9]{3}"))) {

					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				// 支店コード、支店名以外にデータが存在しているか判定
				if (inputData.length > 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				// 支店コードをキーに支店名を格納
				branchDefinitionMap.put(inputData[0], inputData[1]);

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// 支店売上ファイルの読み込み
		FilenameFilter filter = new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.matches("[0-9]{8}+.rcd")) {
					return true;
				} else {
					return false;
				}

			}
		};

		// 支店売上ファイルのフィルタリングを実施
		File[] salseFiles = new File(address).listFiles(filter);

		// 昇順並び替え
		Arrays.sort(salseFiles);

		int fileCnt = 0;

		// 構成チェックと売上集計
		for (File salseFile : salseFiles) {

			try {
				br = new BufferedReader(new FileReader(salseFile));

				int fileNumber = Integer.parseInt(salseFile.getName().substring(0, 8));

				// ファイル名が連番か判定
				if ((fileNumber - fileCnt) != 1) {

					System.out.println("売上ファイル名が連番になっていません");
					return false;
				}

				String branchCode = br.readLine();
				String salseValue = br.readLine();

				// 売上ファイルの支店コードが支店定義に存在するか判定
				if (!(branchDefinitionMap.containsKey(branchCode))) {

					System.out.println(salseFile.getName() + "の支店コードが不正です");
					return false;
				}

				// 3列目以降のデータが存在するか判定
				if (br.readLine() != null) {

					System.out.println(salseFile.getName() + "のフォーマットが不正です");
					return false;
				}

				String[] salseData = { branchCode, salseValue };
				salseArrayList.add(salseData);

				fileCnt++;

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return true;
	}

	/**
	 * 〈支店売上ファイル〉集計メソッド
	 *
	 * @param ArrayList<String[]> 売上ファイルのデータを参照渡し
	 * @param Map<String,Integer> 支店別売上集計ファイル用Mapの参照渡し
	 *
	 * @return boolean 処理結果の有無
	 */
	private static boolean aggregateBranchSalse(ArrayList<String[]> salseArrayList,
			Map<String, Integer> salseMap) {

		int aggregateValue = 0;

		for (String[] salseData : salseArrayList) {

			String branchCode = salseData[0];
			String salseValue = salseData[1];

			// 支店集計
			try {
				aggregateValue = Integer.parseInt(salseValue);

				if (salseMap.containsKey(branchCode)) {
					aggregateValue = salseMap.get(branchCode) + aggregateValue;
				}
			} catch (Exception e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
			// 売り上げが10桁を超えているか判定
			if (String.valueOf(aggregateValue).length() > 9) {
				System.out.println("合計金額が10桁を超えました");
				return false;
			}
			salseMap.put(branchCode, aggregateValue);

		}

		return true;
	}

	/**
	 * 〈売上集計ファイル〉出力メソッド
	 *
	 * @param address 〈売上集計ファイル〉の格納先
	 * @param Map<String,String> 支店定義ファイル用Mapの参照渡し
	 * @param Map<String,Integer> 支店別売上集計ファイル用Mapの参照渡し
	 *
	 * @return boolean 処理結果の有無
	 */
	private static boolean outputFileData(String address, Map<String, String> branchDefinitionMap,
			Map<String, Integer> salseMap) {

		// ファイル出力
		File branchSalesSummaryFile = new File(address, AGGREGATION_BY_BRANCH);

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new BufferedWriter(new FileWriter(branchSalesSummaryFile)));

			for (String branchCode : branchDefinitionMap.keySet()) {

				// 売り上げがない支店は0を格納
				if (salseMap.get(branchCode) == null) {
					salseMap.put(branchCode, 0);
				}

				pw.println(branchCode + "," + branchDefinitionMap.get(branchCode) + "," + salseMap.get(branchCode));

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				pw.close();
			} catch (Exception e) {
			}

		}
		return true;
	}

}
